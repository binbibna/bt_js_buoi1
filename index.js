/** Bài 1
 * Đầu vào : edg1 ( số ngày làm)
 * 
 * Các bước xử lí : gọi trip là hằng số về lương 1 ngày 
 *                      sum là tiền lương 1 tháng 
 *                      sum = trip * edge1 ;
 * 
 * Kết quả ra : tiền lương 1 tháng 
 */
var edg1 = 30; // 30 ngày làm
const trip = 100000; //tiền lương 1 ngày
var sum = trip * edg1; 
console.log("Tổng tiền lương 1 tháng là : ",sum);












/** Bài 2
 * Đầu vào : a,b,c,d,e là 5 số thực;
 * 
 * Các bước xử lí : gọi x là giá trị trung bình của 5 số
 * 
 * 
 * Kết quả ra : giá trị trung bình của 5 số 
 */
var a = 3;
var b = 2;
var c = 6;
var d = 9;
var e= 10;
var x = (a + b + c + e + d)/5;
console.log("Giá trị trung bình là : ",x);














/** Bài 3
 * Đầu vào : gọi usd là số tiền cần quy đổi 
 * 
 * Các bước xử lí : rad là hằng số tiền usd ( rad = 23500) 
 *                  out là số tiền đổi được ( usd * rad)
 * 
 * 
 * Kết quả : số tiền đổi ra được
 */
var usd = 2;
const rad = 23500;
var out = usd * rad;
console.log("Tiền USD :",usd +" Tiền VND :",out);










/** Bài 4
 * Đầu vào : cd chiều dài 
 *           cr chiều rộng
 * 
 * Các bước xử lí : dt : cr * cd
 *                  cv : (cd + cr)*2
 * 
 * Kết quả ra : din tích và chu vi
 */
var cr = 10;
var cd = 20;
var dt = cr*cd;
var cv = (cd+cr)*2;
console.log("diện tích là :",dt);
console.log("chu vi là :",cv);








/** Bài 5
 * Đầu vào : ks là số hay chữ số 
 * 
 * Các bước xử lí : Math.floor(ks/10) lấy dc số hc ( hàng chục)
 *                  dv lấy số đơn vị ks % 10
 *                  all là tổng của số    
 * 
 * Kết quả : tổng số là
 */
var ks = 23;
var hc = Math.floor(ks/10);
var dv = ks % 10;
var all = hc + dv;
console.log("Tổng của số",ks +" là :",all);


